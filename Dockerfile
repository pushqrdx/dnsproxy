FROM python:2.7-alpine

WORKDIR /app

COPY . .

EXPOSE 53

ENTRYPOINT [ "python", "dnsproxy.py" ]
